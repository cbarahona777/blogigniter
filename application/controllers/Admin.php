<?php

class Admin extends CI_Controller{

    //metodo constructor
    public function __construct() {
       parent:: __construct();

       $this->load->helper("url");

    }

    public function index() {
        //cargar la vista
        $this->load->view("admin/test");
    }


    public function post_list() {
        $this->load->view("admin/post_list");
    }

}